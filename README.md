This repository contains a single file, .editorconfig, which specifies the C# coding style rules and conventions for the 
group.

See
https://docs.microsoft.com/en-us/visualstudio/ide/create-portable-custom-editor-options?view=vs-2017

see also
https://docs.microsoft.com/en-us/visualstudio/ide/editorconfig-code-style-settings-reference?view=vs-2017#formatting-conventions

When this file is included in a Visual Studio project, it asserts the C# coding style rules, overriding the
defaults.

If it is placed in a parent directory of projects, it applies to all the projects.  Visual Studio searches up the
directory tree until it finds a .editorconfig file.

Many editors besides Visual Studio will utilize the .editorconfig rules.  Notepad++ will use it, if a 
plugin is installed:
https://bitbucket.org/editorconfig/editorconfig-notepad-plus-plus
